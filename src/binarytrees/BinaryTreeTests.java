package binarytrees;
/**
 * Class:  BinaryTreeTests
 * @author Daniel Plante
 * @version 1.0
 */

import java.util.Iterator;

public class BinaryTreeTests
{
    public static void main(String[] args)
    {
        new BinaryTreeTests();
    }
    
    public BinaryTreeTests()
    {
    	this.binarySearchTest();
    }
    
    public void binarySearchTest()
    {
    	BinarySearchTree<Integer> tree;
    	
    	tree = new BinarySearchTree<Integer>();
    	
		tree.insert(new Integer(6));
		tree.insert(new Integer(46));
		tree.insert(new Integer(9));
		tree.insert(new Integer(-16));
		tree.insert(new Integer(6));
		tree.insert(new Integer(3));
		
		Object object;
		String str, expression;

		Iterator<Integer> it;
		it = tree.iterator();

		expression = "";
		while(it.hasNext())
		{
			object = it.next();
			str = " " + object.toString() + " ";
			expression += str;
		}
		System.out.println("In Order:" + expression + "\n");
	
		
		

		
		
		
		
		

    }  
}