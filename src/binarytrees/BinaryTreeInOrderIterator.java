package binarytrees;
/**
 * Class:    BinaryTreeInOrderIterator
 * Purpose:
 * 
 * Date:     Apr 14, 2004
 * @author   dplante
 * @version  1.0
 *
 */
import java.util.Vector;

public class BinaryTreeInOrderIterator<T> extends BinaryTreeIterator<T>
{
	public BinaryTreeInOrderIterator(BinaryTree<T> tree)
	{
		super(tree);
	}
	
	public void buildTreeVector(BinaryTree<T> tree)
	{
	   T datum;
	   Vector<T> vector;
	   
	   vector = this.getTreeVector();
	   if(tree != null)
	   {
		  datum = tree.getData();
		  this.buildTreeVector(tree.getLeftTree());
		  vector.addElement(datum);
		  this.buildTreeVector(tree.getRightTree());
	   }
	}

}