package binarytrees;
/**
 * Class:  BinaryTreeIterator
 * @author Daniel Plante
 * @version 1.0
 */
import java.util.Iterator;
import java.util.Vector;

public abstract class BinaryTreeIterator<T> implements Iterator<T>
{
	//////////////////////////////////////////////
	//            Properties                    //
	//////////////////////////////////////////////
    private Vector<T> myTreeVector;
    private int myIndex;
    private int mySize;
    
	//////////////////////////////////////////////
	//            Methods                       //
	//////////////////////////////////////////////
	public BinaryTreeIterator(BinaryTree<T> tree)
	{
		myTreeVector = new Vector<T>();
		this.buildTreeVector(tree);
		myTreeVector.trimToSize();
		myIndex = 0;
		mySize = myTreeVector.size();
	}
	
	//////////////////////
	//  Abstract method //
	//////////////////////
	public abstract void buildTreeVector(BinaryTree<T> tree);
	
	public boolean hasNext()
	{
		return myIndex < mySize;
	}
	
	public T next()
	{
		T o;
		
		o = myTreeVector.elementAt(myIndex);
		myIndex++;
		return o;
	}
	
	protected void setTreeVector(Vector<T> vector)
	{
		myTreeVector = vector;
	}
	
	public Vector<T> getTreeVector()
	{
		return myTreeVector;
	}
	
	public void remove()
	{
		// Do nothing, but must override.  To implement, same as
		// delete() in BinarySearchTree.	
	}
}
