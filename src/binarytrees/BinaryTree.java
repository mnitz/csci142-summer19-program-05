package binarytrees;
import java.util.Iterator;

/**
 * Class:  BinaryTree
 * @author Daniel Plante
 * @version 1.0
 * @param <T>
 */

public class BinaryTree<T>
{
   //////////////////////////////////////////////
   //            Properties                    //
   //////////////////////////////////////////////
   private BinaryTree<T> myLeftTree;
   private BinaryTree<T> myRightTree;
   private BinaryTree<T> myParent;
   private T myData;

   //////////////////////////////////////////////
   //            Methods                       //
   //////////////////////////////////////////////
   public BinaryTree()
   {
   	  myData = null;
	  myParent = null;
	  myLeftTree = null;
	  myRightTree = null;
   }

   public BinaryTree(T o)
   {
   	  myData = o;
	  myParent = null;
	  myLeftTree = null;
	  myRightTree = null;
   }

   public BinaryTree(T datum, BinaryTree<T> left, BinaryTree<T> right)
   {
      this.setParent(null);
      this.setData(datum);
      this.setLeftTree(left);
      this.setRightTree(right);
   }

   public Iterator<T> iterator()
   {
	   BinaryTreeInOrderIterator<T> inIt;

	   inIt = new BinaryTreeInOrderIterator<T> (this);
	   return inIt;
   }
	
   public boolean hasLeftTree()
   {
      return myLeftTree != null;
   }
  
   public boolean hasRightTree()
   {
      return myRightTree != null;
   }

   public boolean isLeafNode()
   {
      boolean flag;
      flag = false;
      if (null == myRightTree &&
          null == myLeftTree)
      {
         flag = true;
      }
      return flag;
   }
  
   public void setLeftTree(BinaryTree<T> tree)
   {
      if(this.hasLeftTree())
      {
         BinaryTree<T> leftParent = myLeftTree.getParent();
         if (leftParent == this)
         {
            myLeftTree.setParent(null);
         }
      }
      
      myLeftTree = tree;
      if(tree != null)
      {
         tree.setParent(this);
      }
   }

   public void setRightTree(BinaryTree<T> tree)
   {
      if(this.hasRightTree())
      {
         BinaryTree<T> rightParent = myRightTree.getParent();
         if (rightParent == this)
         {
            myRightTree.setParent(null);
         }
      }
      
      myRightTree = tree;
      if(tree != null)
      {
         tree.setParent(this);
      }
   }

   public BinaryTree<T> getLeftTree()
   {
      return myLeftTree;
   }

   public BinaryTree<T> getRightTree()
   {
      return myRightTree;
   }

   public void setParent(BinaryTree<T> tree)
   {
      myParent = tree;
   }

   public BinaryTree<T> getParent()
   {
      return myParent;
   }

   public void setData(T data)
   {
      myData = data;
   }

   public T getData()
   {
      return myData;
   }
}