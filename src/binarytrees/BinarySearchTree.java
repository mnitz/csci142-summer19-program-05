package binarytrees;
import java.util.ArrayList;
/**
 * Class:    BinarySearchTree
 * Purpose:
 * 
 * Date:     Apr 16, 2004
 * @author   dplante
 * @version  1.0
 *
 */
import java.util.Comparator;
import java.util.Iterator;

public class BinarySearchTree<T> extends BinaryTree<T>
{
	//////////////////////////////////////////////
	//            Properties                    //
	//////////////////////////////////////////////
    private Comparator<Object> myComparator;
    
	//////////////////////////////////////////////
	//            Methods                       //
	//////////////////////////////////////////////
	public BinarySearchTree()
	{
		super();
		myComparator = new NaturalComparator();
	}
	
	public BinarySearchTree(T datum)
	{
		super(datum);
		myComparator = new NaturalComparator();
	}
  
	public BinarySearchTree(T datum, BinarySearchTree<T> left, 
                                          BinarySearchTree<T> right)
	{
		super(datum, left, right);
		myComparator = new NaturalComparator();
	}
	
	public void add(T o)
	{
		this.insert(o, this);
	}
	
	public void remove(T o)
	{
		this.delete(o, this);
	}
	
	public boolean contains(T o)
	{

		Iterator<T> it;
		it = this.iterator();
		
		while(it.hasNext()) 
		{
			if(o.equals(it.next()))
				return true;
		}
		
		
		return false;
	}
	
	public void insert(T o)
	{
		this.insert(o, this);
	}
	
	private void insert(T o, BinarySearchTree<T> root)
	{
	    BinarySearchTree<T> node;
	    T rootData;
	    int check;
	    
	    if(root == null)
	    {
	        return;
	    }
	    
	    rootData = root.getData();
	    check = 0;
	    node = null;
	    
	    if(rootData == null)
	    {
	        this.setData(o);
	    }
	    else
	    {
	        check = myComparator.compare(o, rootData);
	        if(check < 0)
	        {
	            node = (BinarySearchTree<T>) root.getLeftTree();
	            if (node == null)
	            {
	                node = new BinarySearchTree<T>(o);
	                node.setParent(root);
	                root.setLeftTree(node);
	                return;
	            }
	            this.insert(o, node);
	        }
	        else
	        {
	            node = (BinarySearchTree<T>) root.getRightTree();
	            if(node == null)
	            {
	                node = new BinarySearchTree<T>(o);
	                node.setParent(root);
	                root.setRightTree(node);
	                return;
	            }
	            this.insert(o, node);
	        }
	    }
	}
	
	public void delete(T o)
	{
		this.delete(o, this);
	}
	
	private void delete(T o, BinarySearchTree<T> root)
	{
		BinarySearchTree<T> search = root.search(o);
		
		int numOfChildren = 0;
		if(search.hasLeftTree())
			numOfChildren++;
		if(search.hasRightTree())
			numOfChildren++;
		if(search != null)
		{
			if(search.getParent() != null)
			{	
				
				if(numOfChildren == 0)
				{
					if(search.getParent().getLeftTree() == search)
						search.getParent().getLeftTree().setParent(null);
					else if(search.getParent().getRightTree() == search)
						search.getParent().getRightTree().setParent(null);
				}
				else if(numOfChildren == 1)
				{
					if(search.getParent().getLeftTree() == search)
					{
						if(search.hasLeftTree())
							search.getParent().setLeftTree(search.getLeftTree());
						else if(search.hasRightTree())
							search.getParent().setLeftTree(search.getRightTree());
					}
					else if(search.getParent().getRightTree() == search)
					{
						if(search.hasRightTree())
							search.getParent().setRightTree(search.getRightTree());
						else if(search.hasLeftTree())
							search.getParent().setRightTree(search.getLeftTree());
					}
				}
				else if(numOfChildren == 2)
				{
					if(search.getParent().getRightTree() == search)
					{
						search.getParent().setRightTree(search.getLeftTree());
						BinaryTree<T> currentNode = search.getLeftTree();
						while(currentNode.getRightTree() != null)
						{
							currentNode = currentNode.getRightTree();
						}
						search.getRightTree().setParent(currentNode);
					}
					if(search.getParent().getLeftTree() == search)
					{
						search.getParent().setLeftTree(search.getRightTree());
						BinaryTree<T> currentNode = search.getRightTree();
						while(currentNode.getLeftTree() != null)
						{
							currentNode = currentNode.getLeftTree();
						}
						currentNode.setLeftTree(search.getLeftTree());
					}
						
				}
			}
			else //now the node being deleted is the top
			{
				BinaryTree<T> currentNode = search.getLeftTree();
				while(currentNode.getLeftTree() != null)
				{
					currentNode = currentNode.getRightTree();
				}	
				search.getRightTree().setParent(currentNode);
			}
		}
		

	}
	
	public BinarySearchTree<T> search(T o)
	{
		return search(o, this);
	}
	
	private BinarySearchTree<T> search(T o, BinarySearchTree<T> root)
	{

			int lookingFor = Integer.parseInt(o.toString());
			int checkingAgainst = Integer.parseInt(root.getData().toString());
			if(lookingFor == checkingAgainst)
				return root;
			
			else if(lookingFor > checkingAgainst)
				search(o, (BinarySearchTree<T>) root.getRightTree());
			else if(lookingFor < checkingAgainst)
				search(o, (BinarySearchTree<T>) root.getLeftTree());
	
		return null;
	

	}
	
	public Object[] toOrderedArray()
	{
		ArrayList<T> someArrayList = new ArrayList<T>();
		BinaryTreeInOrderIterator<T> iterator;
		iterator = new BinaryTreeInOrderIterator<T>(this);
		while(iterator.hasNext())
		{
			someArrayList.add(iterator.next());
		}
		return someArrayList.toArray();
	}
}
