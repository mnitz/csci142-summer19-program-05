package testcases;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import binarytrees.*;

public class BinaryTreeTestCases
{
	private BinarySearchTree<Integer> myTree;
	
	@Before
	public void setup()
	{
    	
		myTree = new BinarySearchTree<Integer>();
		
		myTree.insert(new Integer(6));
		myTree.insert(new Integer(46));
		myTree.insert(new Integer(9));
		myTree.insert(new Integer(-16));
		myTree.insert(new Integer(6));
		myTree.insert(new Integer(3));
		
		myTree.insert(new Integer(69));
		myTree.insert(new Integer(-94));
		myTree.insert(new Integer(-71));
		myTree.insert(new Integer(-72));
		myTree.insert(new Integer(79));
		myTree.insert(new Integer(38));
		
		myTree.insert(new Integer(43));
		myTree.insert(new Integer(-81));
		myTree.insert(new Integer(30));
		myTree.insert(new Integer(-26));
		myTree.insert(new Integer(52));
		myTree.insert(new Integer(4));
		
		
		
		//tree has 18 nodes
	}
	
	@Test
	public void testToOrderedArray()
	{
		String myString = "";
		Object[] myArray = myTree.toOrderedArray();
		for(int i = 0; i < myArray.length; i++)
		{
			myString = myString + " " + myArray[i];
		}
		myString = myString.trim();
		boolean theTruth = myString.equals("-94 -81 -72 -71 -26 -16 3 4 6 6 9 30 38 43 46 52 69 79");
		assertTrue("Ordered array should match array in test", theTruth);
	}
	
	@Test
	public void searchForThirty()
	{
		System.out.println(myTree.search(-81));

	}
	
	@Test
	public void searchForDNE()
	{
		BinarySearchTree<Integer> sixeight = myTree.search(68);
		assertTrue("Search for 68 should return null as it does not exist", sixeight == null);
	}
	
	@Test
	public void searchFromEmptyTree()
	{
		BinarySearchTree<Integer> tempTree;
		tempTree = new BinarySearchTree<Integer>();
		BinarySearchTree<Integer> emptySearch = tempTree.search(94);
		assertTrue("Search an empty tree should return null", emptySearch == null);
	}
	
	@Test
	public void deleteFromEmptyTree()
	{
		BinarySearchTree<Integer> tempTree;
		tempTree = new BinarySearchTree<Integer>();
		try {
		tempTree.delete(3);
		}
		catch(Exception NullPointerException)
		{
			assertTrue("Delete from empty tree should make searching for node null",
					tempTree.search(3) == null);
		}
	}
	
	@Test
	public void deleteSingleDigit()
	{
		myTree.delete(-94);
		String myString = "";
		Object[] myArray = myTree.toOrderedArray();
		for(int i = 0; i < myArray.length; i++)
		{
			myString = myString + " " + myArray[i];
		}
		myString = myString.trim();
//		System.out.println(myString);
		boolean theTruth = myString.equals("-94 -81 -72 -71 -26 -16 3 4 6 9 30 38 43 46 52 69 79");
		assertTrue("Ordered array should match array in test with one less 6", theTruth);
	}
	
	
	
	
	
	
}
